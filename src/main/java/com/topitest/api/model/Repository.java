package com.topitest.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Repository {
    private int id;
    private String name;
    private String full_name;
    private RepositoryOwner owner;
    private Boolean privateRepo;
    private String html_url;
    private String description;
    private Boolean fork;
    private String url;
    private String forks_url;
    private String keys_url;
    private String collaborators_url;
    private String teams_url;
    private String hooks_url;
    private String issue_events_url;
    private String events_url;
    private String assignees_url;
    private String branches_url;
    private String tags_url;
    private String blobs_url;
    private String git_tags_url;
    private String git_refs_url;
    private String trees_url;
    private String statuses_url;
    private String languages_url;
    private String stargazers_url;
    private String contributors_url;
    private String subscribers_url;
    private String subscription_url;
    private String commits_url;
    private String git_commits_url;
    private String comments_url;
    private String issue_comment_url;
    private String contents_url;
    private String compare_url;
    private String merges_url;
    private String archive_url;
    private String downloads_url;
    private String issues_url;
    private String pulls_url;
    private String milestones_url;
    private String notifications_url;
    private String labels_url;
    private String releases_url;
    private String deployments_url;
    private String created_at;
    private String updated_at;
    private String pushed_at;
    private String git_url;
    private String ssh_url;
    private String clone_url;
    private String svn_url;
    private String homepage;
    private int size;
    private int stargazers_count;
    private int watchers_count;
    private String language;
    private Boolean has_issues;
    private Boolean has_projects;
    private Boolean has_downloads;
    private Boolean has_wiki;
    private Boolean has_pages;
    private int forks_count;
    private String mirror_url;
    private Boolean archived;
    private int open_issues_count;
    private License license;
    private int forks;
    private int open_issues;
    private int watchers;
    private String default_branch;
    private int score;

    public Repository() {}

    public Repository(
        int id,
        String name,
        String full_name,
        RepositoryOwner owner,
        Boolean privateRepo,
        String html_url,
        String description,
        Boolean fork,
        String url,
        String forks_url,
        String keys_url,
        String collaborators_url,
        String teams_url,
        String hooks_url,
        String issue_events_url,
        String events_url,
        String assignees_url,
        String branches_url,
        String tags_url,
        String blobs_url,
        String git_tags_url,
        String git_refs_url,
        String trees_url,
        String statuses_url,
        String languages_url,
        String stargazers_url,
        String contributors_url,
        String subscribers_url,
        String subscription_url,
        String commits_url,
        String git_commits_url,
        String comments_url,
        String issue_comment_url,
        String contents_url,
        String compare_url,
        String merges_url,
        String archive_url,
        String downloads_url,
        String issues_url,
        String pulls_url,
        String milestones_url,
        String notifications_url,
        String labels_url,
        String releases_url,
        String deployments_url,
        String created_at,
        String updated_at,
        String pushed_at,
        String git_url,
        String ssh_url,
        String clone_url,
        String svn_url,
        String homepage,
        int size,
        int stargazers_count,
        int watchers_count,
        String language,
        Boolean has_issues,
        Boolean has_projects,
        Boolean has_downloads,
        Boolean has_wiki,
        Boolean has_pages,
        int forks_count,
        String mirror_url,
        Boolean archived,
        int open_issues_count,
        License license,
        int forks,
        int open_issues,
        int watchers,
        String default_branch,
        int score
    ) {
        this.id = id;
        this.name = name;
        this.full_name = full_name;
        this.owner = owner;
        this.privateRepo = privateRepo;
        this.html_url = html_url;
        this.description = description;
        this.fork = fork;
        this.url = url;
        this.forks_url = forks_url;
        this.keys_url = keys_url;
        this.collaborators_url = collaborators_url;
        this.teams_url = teams_url;
        this.hooks_url = hooks_url;
        this.issue_events_url = issue_events_url;
        this.events_url = events_url;
        this.assignees_url = assignees_url;
        this.branches_url = branches_url;
        this.tags_url = tags_url;
        this.blobs_url = blobs_url;
        this.git_tags_url = git_tags_url;
        this.git_refs_url = git_refs_url;
        this.trees_url = trees_url;
        this.statuses_url = statuses_url;
        this.languages_url = languages_url;
        this.stargazers_url = stargazers_url;
        this.contributors_url = contributors_url;
        this.subscribers_url = subscribers_url;
        this.subscription_url = subscription_url;
        this.commits_url = commits_url;
        this.git_commits_url = git_commits_url;
        this.comments_url = comments_url;
        this.issue_comment_url = issue_comment_url;
        this.contents_url = contents_url;
        this.compare_url = compare_url;
        this.merges_url = merges_url;
        this.archive_url = archive_url;
        this.downloads_url = downloads_url;
        this.issues_url = issues_url;
        this.pulls_url = pulls_url;
        this.milestones_url = milestones_url;
        this.notifications_url = notifications_url;
        this.labels_url = labels_url;
        this.releases_url = releases_url;
        this.deployments_url = deployments_url;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.pushed_at = pushed_at;
        this.git_url = git_url;
        this.ssh_url = ssh_url;
        this.clone_url = clone_url;
        this.svn_url = svn_url;
        this.homepage = homepage;
        this.size = size;
        this.stargazers_count = stargazers_count;
        this.watchers_count = watchers_count;
        this.language = language;
        this.has_issues = has_issues;
        this.has_projects = has_projects;
        this.has_downloads = has_downloads;
        this.has_wiki = has_wiki;
        this.has_pages = has_pages;
        this.forks_count = forks_count;
        this.mirror_url = mirror_url;
        this.archived = archived;
        this.open_issues_count = open_issues_count;
        this.license = license;
        this.forks = forks;
        this.open_issues = open_issues;
        this.watchers = watchers;
        this.default_branch = default_branch;
        this.score = score;
    }

    @JsonProperty("fullName")
    public String getFull_name() {
        return full_name;
    }
    
    @JsonProperty("full_name")
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    @JsonProperty("owner")
    public RepositoryOwner getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(RepositoryOwner owner) {
        this.owner = owner;
    }

    @JsonProperty("privateRepo")
    public Boolean getPrivateRepo() {
        return privateRepo;
    }
    
    @JsonProperty("private")
    public void setPrivateRepo(Boolean private_repo) {
        this.privateRepo = private_repo;
    }

    @JsonProperty("htmlUrl")
    public String getHtml_url() {
        return html_url;
    }

    @JsonProperty("html_url")
    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("fork")
    public Boolean getFork() {
        return fork;
    }

    @JsonProperty("fork")
    public void setFork(Boolean fork) {
        this.fork = fork;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("forksUrl")
    public String getForks_url() {
        return forks_url;
    }

    @JsonProperty("forks_url")
    public void setForks_url(String forks_url) {
        this.forks_url = forks_url;
    }

    @JsonProperty("keysUrl")
    public String getKeys_url() {
        return keys_url;
    }

    @JsonProperty("keys_url")
    public void setKeys_url(String keys_url) {
        this.keys_url = keys_url;
    }

    @JsonProperty("collaboratorsUrl")
    public String getCollaborators_url() {
        return collaborators_url;
    }

    @JsonProperty("collaborators_url")
    public void setCollaborators_url(String collaborators_url) {
        this.collaborators_url = collaborators_url;
    }

    @JsonProperty("teamsUrl")
    public String getTeams_url() {
        return teams_url;
    }

    @JsonProperty("teams_url")
    public void setTeams_url(String teams_url) {
        this.teams_url = teams_url;
    }

    @JsonProperty("hooksUrl")
    public String getHooks_url() {
        return hooks_url;
    }

    @JsonProperty("hooks_url")
    public void setHooks_url(String hooks_url) {
        this.hooks_url = hooks_url;
    }

    @JsonProperty("issueEventsUrl")
    public String getIssue_events_url() {
        return issue_events_url;
    }

    @JsonProperty("issue_events_url")
    public void setIssue_events_url(String issue_events_url) {
        this.issue_events_url = issue_events_url;
    }

    @JsonProperty("eventsUrl")
    public String getEvents_url() {
        return events_url;
    }

    @JsonProperty("events_url")
    public void setEvents_url(String events_url) {
        this.events_url = events_url;
    }

    @JsonProperty("assigneesUrl")
    public String getAssignees_url() {
        return assignees_url;
    }

    @JsonProperty("assignees_url")
    public void setAssignees_url(String assignees_url) {
        this.assignees_url = assignees_url;
    }

    @JsonProperty("branchesUrl")
    public String getBranches_url() {
        return branches_url;
    }

    @JsonProperty("branches_url")
    public void setBranches_url(String branches_url) {
        this.branches_url = branches_url;
    }

    @JsonProperty("tagsUrl")
    public String getTags_url() {
        return tags_url;
    }

    @JsonProperty("tags_url")
    public void setTags_url(String tags_url) {
        this.tags_url = tags_url;
    }

    @JsonProperty("blobsUrl")
    public String getBlobs_url() {
        return blobs_url;
    }

    @JsonProperty("blobs_url")
    public void setBlobs_url(String blobs_url) {
        this.blobs_url = blobs_url;
    }

    @JsonProperty("gitTagsUrl")
    public String getGit_tags_url() {
        return git_tags_url;
    }

    @JsonProperty("git_tags_url")
    public void setGit_tags_url(String git_tags_url) {
        this.git_tags_url = git_tags_url;
    }

    @JsonProperty("gitRefsUrl")
    public String getGit_refs_url() {
        return git_refs_url;
    }

    @JsonProperty("git_refs_url")
    public void setGit_refs_url(String git_refs_url) {
        this.git_refs_url = git_refs_url;
    }

    @JsonProperty("treesUrl")
    public String getTrees_url() {
        return trees_url;
    }

    @JsonProperty("trees_url")
    public void setTrees_url(String trees_url) {
        this.trees_url = trees_url;
    }

    @JsonProperty("statusesUrl")
    public String getStatuses_url() {
        return statuses_url;
    }

    @JsonProperty("statuses_url")
    public void setStatuses_url(String statuses_url) {
        this.statuses_url = statuses_url;
    }

    @JsonProperty("languagesUrl")
    public String getLanguages_url() {
        return languages_url;
    }

    @JsonProperty("languages_url")
    public void setLanguages_url(String languages_url) {
        this.languages_url = languages_url;
    }

    @JsonProperty("stargazersUrl")
    public String getStargazers_url() {
        return stargazers_url;
    }

    @JsonProperty("stargazers_url")
    public void setStargazers_url(String stargazers_url) {
        this.stargazers_url = stargazers_url;
    }

    @JsonProperty("contributorsUrl")
    public String getContributors_url() {
        return contributors_url;
    }

    @JsonProperty("contributors_url")
    public void setContributors_url(String contributors_url) {
        this.contributors_url = contributors_url;
    }

    @JsonProperty("subscribersUrl")
    public String getSubscribers_url() {
        return subscribers_url;
    }

    @JsonProperty("subscribers_url")
    public void setSubscribers_url(String subscribers_url) {
        this.subscribers_url = subscribers_url;
    }

    @JsonProperty("subscriptionUrl")
    public String getSubscription_url() {
        return subscription_url;
    }

    @JsonProperty("subscription_url")
    public void setSubscription_url(String subscription_url) {
        this.subscription_url = subscription_url;
    }

    @JsonProperty("commitsUrl")
    public String getCommits_url() {
        return commits_url;
    }

    @JsonProperty("commits_url")
    public void setCommits_url(String commits_url) {
        this.commits_url = commits_url;
    }

    @JsonProperty("gitCommitsUrl")
    public String getGit_commits_url() {
        return git_commits_url;
    }

    @JsonProperty("git_commits_url")
    public void setGit_commits_url(String git_commits_url) {
        this.git_commits_url = git_commits_url;
    }

    @JsonProperty("commentsUrl")
    public String getComments_url() {
        return comments_url;
    }

    @JsonProperty("comments_url")
    public void setComments_url(String comments_url) {
        this.comments_url = comments_url;
    }

    @JsonProperty("issueCommentUrl")
    public String getIssue_comment_url() {
        return issue_comment_url;
    }

    @JsonProperty("issue_comment_url")
    public void setIssue_comment_url(String issueComment_url) {
        this.issue_comment_url = issueComment_url;
    }

    @JsonProperty("contentsUrl")
    public String getContents_url() {
        return contents_url;
    }

    @JsonProperty("contents_url")
    public void setContents_url(String contents_url) {
        this.contents_url = contents_url;
    }

    @JsonProperty("compareUrl")
    public String getCompare_url() {
        return compare_url;
    }

    @JsonProperty("compare_url")
    public void setCompare_url(String compare_url) {
        this.compare_url = compare_url;
    }

    @JsonProperty("mergesUrl")
    public String getMerges_url() {
        return merges_url;
    }

    @JsonProperty("merges_url")
    public void setMerges_url(String merges_url) {
        this.merges_url = merges_url;
    }

    @JsonProperty("archiveUrl")
    public String getArchive_url() {
        return archive_url;
    }

    @JsonProperty("archive_url")
    public void setArchive_url(String archive_url) {
        this.archive_url = archive_url;
    }

    @JsonProperty("downloadsUrl")
    public String getDownloads_url() {
        return downloads_url;
    }

    @JsonProperty("downloads_url")
    public void setDownloads_url(String downloads_url) {
        this.downloads_url = downloads_url;
    }

    @JsonProperty("issuesUrl")
    public String getIssues_url() {
        return issues_url;
    }

    @JsonProperty("issues_url")
    public void setIssues_url(String issues_url) {
        this.issues_url = issues_url;
    }

    @JsonProperty("pullsUrl")
    public String getPulls_url() {
        return pulls_url;
    }

    @JsonProperty("pulls_url")
    public void setPulls_url(String pulls_url) {
        this.pulls_url = pulls_url;
    }

    @JsonProperty("milestonesUrl")
    public String getMilestones_url() {
        return milestones_url;
    }

    @JsonProperty("milestones_url")
    public void setMilestones_url(String milestones_url) {
        this.milestones_url = milestones_url;
    }

    @JsonProperty("notificationsUrl")
    public String getNotifications_url() {
        return notifications_url;
    }

    @JsonProperty("notifications_url")
    public void setNotifications_url(String notifications_url) {
        this.notifications_url = notifications_url;
    }

    @JsonProperty("labelsUrl")
    public String getLabels_url() {
        return labels_url;
    }

    @JsonProperty("labels_url")
    public void setLabels_url(String labels_url) {
        this.labels_url = labels_url;
    }

    @JsonProperty("releasesUrl")
    public String getReleases_url() {
        return releases_url;
    }

    @JsonProperty("releases_url")
    public void setReleases_url(String releases_url) {
        this.releases_url = releases_url;
    }

    @JsonProperty("deploymentsUrl")
    public String getDeployments_url() {
        return deployments_url;
    }

    @JsonProperty("deployments_url")
    public void setDeployments_url(String deployments_url) {
        this.deployments_url = deployments_url;
    }

    @JsonProperty("createdAt")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("updatedAt")
    public String getUpdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("pushedAt")
    public String getPushed_at() {
        return pushed_at;
    }

    @JsonProperty("pushed_at")
    public void setPushed_at(String pushed_at) {
        this.pushed_at = pushed_at;
    }

    @JsonProperty("gitUrl")
    public String getGit_url() {
        return git_url;
    }

    @JsonProperty("git_url")
    public void setGit_url(String git_url) {
        this.git_url = git_url;
    }

    @JsonProperty("sshUrl")
    public String getSsh_url() {
        return ssh_url;
    }

    @JsonProperty("ssh_url")
    public void setSsh_url(String ssh_url) {
        this.ssh_url = ssh_url;
    }

    @JsonProperty("cloneUrl")
    public String getClone_url() {
        return clone_url;
    }

    @JsonProperty("clone_url")
    public void setClone_url(String clone_url) {
        this.clone_url = clone_url;
    }

    @JsonProperty("svnUrl")
    public String getSvn_url() {
        return svn_url;
    }

    @JsonProperty("svn_url")
    public void setSvn_url(String svn_url) {
        this.svn_url = svn_url;
    }

    @JsonProperty("homepage")
    public String getHomepage() {
        return homepage;
    }

    @JsonProperty("homepage")
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @JsonProperty("size")
    public int getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(int size) {
        this.size = size;
    }

    @JsonProperty("stargazersCount")
    public int getStargazers_count() {
        return stargazers_count;
    }

    @JsonProperty("stargazers_count")
    public void setStargazers_count(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    @JsonProperty("watchersCount")
    public int getWatchers_count() {
        return watchers_count;
    }

    @JsonProperty("watchers_count")
    public void setWatchers_count(int watchers_count) {
        this.watchers_count = watchers_count;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("hasIssues")
    public Boolean getHas_issues() {
        return has_issues;
    }

    @JsonProperty("has_issues")
    public void setHas_issues(Boolean has_issues) {
        this.has_issues = has_issues;
    }

    @JsonProperty("hasProjects")
    public Boolean getHas_projects() {
        return has_projects;
    }

    @JsonProperty("has_projects")
    public void setHas_projects(Boolean has_projects) {
        this.has_projects = has_projects;
    }

    @JsonProperty("hasDownloads")
    public Boolean getHas_downloads() {
        return has_downloads;
    }

    @JsonProperty("has_downloads")
    public void setHas_downloads(Boolean has_downloads) {
        this.has_downloads = has_downloads;
    }

    @JsonProperty("hasWiki")
    public Boolean getHas_wiki() {
        return has_wiki;
    }

    @JsonProperty("has_wiki")
    public void setHas_wiki(Boolean has_wiki) {
        this.has_wiki = has_wiki;
    }

    @JsonProperty("hasPages")
    public Boolean getHas_pages() {
        return has_pages;
    }

    @JsonProperty("has_pages")
    public void setHas_pages(Boolean has_pages) {
        this.has_pages = has_pages;
    }

    @JsonProperty("forksCount")
    public int getForks_count() {
        return forks_count;
    }

    @JsonProperty("forks_count")
    public void setForks_count(int forks_count) {
        this.forks_count = forks_count;
    }

    @JsonProperty("mirrorUrl")
    public String getMirror_url() {
        return mirror_url;
    }

    @JsonProperty("mirror_url")
    public void setMirror_url(String mirror_url) {
        this.mirror_url = mirror_url;
    }

    @JsonProperty("archived")
    public Boolean getArchived() {
        return archived;
    }

    @JsonProperty("archived")
    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    @JsonProperty("openIssuesCount")
    public int getOpen_issues_count() {
        return open_issues_count;
    }

    @JsonProperty("open_issues_count")
    public void setOpen_issues_count(int open_issues_count) {
        this.open_issues_count = open_issues_count;
    }

    @JsonProperty("license")
    public License getLicense() {
        return license;
    }

    @JsonProperty("license")
    public void setLicense(License license) {
        this.license = license;
    }

    @JsonProperty("forks")
    public int getForks() {
        return forks;
    }

    @JsonProperty("forks")
    public void setForks(int forks) {
        this.forks = forks;
    }

    @JsonProperty("openIssues")
    public int getOpen_issues() {
        return open_issues;
    }

    @JsonProperty("open_issues")
    public void setOpen_issues(int open_issues) {
        this.open_issues = open_issues;
    }

    @JsonProperty("watchers")
    public int getWatchers() {
        return watchers;
    }

    @JsonProperty("watchers")
    public void setWatchers(int watchers) {
        this.watchers = watchers;
    }

    @JsonProperty("defaultBranch")
    public String getDefault_branch() {
        return default_branch;
    }

    @JsonProperty("default_branch")
    public void setDefault_branch(String default_branch) {
        this.default_branch = default_branch;
    }

    @JsonProperty("score")
    public int getScore() {
        return score;
    }

    @JsonProperty("score")
    public void setScore(int score) {
        this.score = score;
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }
}