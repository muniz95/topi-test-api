package com.topitest.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class License {
    private String key;
    private String name;
    private String spdxId;
    private String url;

    public License() {}

    @JsonProperty("key")
    public String getKey() {
        return key;
    }
    
    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("spdxId")
    public String getSpdxId() {
        return spdxId;
    }

    @JsonProperty("spdx_id")
    public void setSpdxId(String spdxId) {
        this.spdxId = spdxId;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }
}