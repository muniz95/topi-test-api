package com.topitest.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RepositoryOwner {
    private String login;
    private int id;
    private String avatar_url;
    private String gravatar_id;
    private String url;
    private String html_url;
    private String followers_url;
    private String following_url;
    private String gists_url;
    private String starred_url;
    private String subscriptions_url;
    private String organizations_url;
    private String repos_url;
    private String events_url;
    private String received_events_url;
    private String type;
    private Boolean site_admin;

    @JsonProperty("login")
    public String getLogin() {
        return login;
    }

    @JsonProperty("login")
    public void setLogin(String login) {
        this.login = login;
    }

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("avatarUrl")
    public String getAvatar_url() {
        return avatar_url;
    }

    @JsonProperty("avatar_url")
    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    @JsonProperty("gravatarId")
    public String getGravatar_id() {
        return gravatar_id;
    }

    @JsonProperty("gravatar_id")
    public void setGravatar_id(String gravatarid) {
        this.gravatar_id = gravatarid;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("htmlUrl")
    public String getHtml_url() {
        return html_url;
    }

    @JsonProperty("html_url")
    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    @JsonProperty("followersUrl")
    public String getFollowers_url() {
        return followers_url;
    }

    @JsonProperty("followers_url")
    public void setFollowers_url(String followers_url) {
        this.followers_url = followers_url;
    }

    @JsonProperty("followingUrl")
    public String getFollowing_url() {
        return following_url;
    }

    @JsonProperty("following_url")
    public void setFollowing_url(String following_url) {
        this.following_url = following_url;
    }

    @JsonProperty("gistsUrl")
    public String getGists_url() {
        return gists_url;
    }

    @JsonProperty("gists_url")
    public void setGists_url(String gists_url) {
        this.gists_url = gists_url;
    }

    @JsonProperty("starredUrl")
    public String getStarred_url() {
        return starred_url;
    }

    @JsonProperty("starred_url")
    public void setStarred_url(String starred_url) {
        this.starred_url = starred_url;
    }

    @JsonProperty("subscriptionsUrl")
    public String getSubscriptions_url() {
        return subscriptions_url;
    }

    @JsonProperty("subscriptions_url")
    public void setSubscriptions_url(String subscriptions_url) {
        this.subscriptions_url = subscriptions_url;
    }

    @JsonProperty("organizationsUrl")
    public String getOrganizations_url() {
        return organizations_url;
    }

    @JsonProperty("organizations_url")
    public void setOrganizations_url(String organizations_url) {
        this.organizations_url = organizations_url;
    }

    @JsonProperty("reposUrl")
    public String getRepos_url() {
        return repos_url;
    }

    @JsonProperty("repos_url")
    public void setRepos_url(String repos_url) {
        this.repos_url = repos_url;
    }

    @JsonProperty("eventsUrl")
    public String getEvents_url() {
        return events_url;
    }

    @JsonProperty("events_url")
    public void setEvents_url(String events_url) {
        this.events_url = events_url;
    }

    @JsonProperty("receivedEventsUrl")
    public String getReceived_events_url() {
        return received_events_url;
    }

    @JsonProperty("received_events_url")
    public void setReceived_events_url(String received_events_url) {
        this.received_events_url = received_events_url;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("siteAdmin")
    public Boolean getSite_admin() {
        return site_admin;
    }

    @JsonProperty("site_admin")
    public void setSite_admin(Boolean siteAdmin) {
        this.site_admin = siteAdmin;
    }
}