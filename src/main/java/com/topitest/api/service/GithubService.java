package com.topitest.api.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.topitest.api.model.Repository;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by rodrigo on 13/12/17.
 */

@Service
public class GithubService {

    RestTemplate restTemplate;

    public GithubService() {
        this.restTemplate = new RestTemplate();
    }

    public Repository[] getRepositoriesInfo(String language) throws IOException {
        String url = "https://api.github.com/search/repositories?q=language:" + language + "&sort=stars&page=1";
        ObjectMapper mapper = new ObjectMapper();
        String jsonStr = this.restTemplate.getForObject(url, String.class);
        Repository[] repos;
        try {
            JsonNode repo = mapper.readTree(jsonStr).get("items");
            repos = mapper.convertValue(repo, new TypeReference<Repository[]>() {});
        } catch (IOException e) {
            throw e;
        }
        return repos;
    }
}
