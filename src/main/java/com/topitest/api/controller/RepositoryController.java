package com.topitest.api.controller;

import com.topitest.api.model.Repository;
import com.topitest.api.service.GithubService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.io.IOException;

@RestController
public class RepositoryController {

    @CrossOrigin(origins = "https://topi-test-rodrigomuniz.surge.sh")
    @RequestMapping(value = "/repos",
            method = RequestMethod.GET,
            produces = {"application/json"})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    Repository[] repos(@RequestParam(value = "language") String language) throws IOException {
        Repository[] reposInfo = new GithubService().getRepositoriesInfo(language);
        return reposInfo;
    }
}
