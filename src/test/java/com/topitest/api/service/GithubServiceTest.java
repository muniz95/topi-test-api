package com.topitest.api.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

/**
 * 
 * Shows how to test a router; the router is configured to route to direct
 * channel names. The configuration would
 * be a fragment of a larger flow. Since the output channels are direct,
 * they have no subscribers outside the context of a larger flow. So, 
 * in this test case, we bridge them to {@link QueueChannel}s to
 * facilitate easy testing.
 * 
 * @author Gary Russell
 * @since 2.0.2
 */
// @RunWith(SpringJUnit4ClassRunner.class)
public class GithubServiceTest {
	
	@Test
	public void doesNotThrowErrosOnSimpleCall() {
		GithubService service = new GithubService();
		boolean thrown = false;

		try {
			service.getRepositoriesInfo("Java");
		} catch (IOException e) {
			thrown = true;
		}

		assertTrue(!thrown);
	}
}